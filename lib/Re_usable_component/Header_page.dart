import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

enum Currency { INR, USD }

class Header_page extends StatefulWidget {
  const Header_page({Key? key}) : super(key: key);

  @override
  _Header_pageState createState() => _Header_pageState();
}

class _Header_pageState extends State<Header_page> {

  Currency _selectedCurrency = Currency.INR;
  late String formattedNumber;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.formattedNumber = NumberFormat.decimalPattern().format(1245.6789);

  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 15,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('hi ehi,',
                style: TextStyle(
                    color: Colors.grey.shade700,
                    fontSize: 18,
                    fontWeight: FontWeight.w500)),
            Icon(Icons.notifications_active)
          ],
        ),
        SizedBox(
          height: 15,
        ),
        Text('${this.formattedNumber}',
            style: TextStyle(
                color: Colors.black,
                fontSize: 33,
                fontWeight: FontWeight.bold)),
        SizedBox(
          height: 15,
        ),
        Container(
          child: DropdownButton(
            value: _selectedCurrency,
            items: Currency.values
                .map(
                  (value) => DropdownMenuItem(
                value: value,
                child: Row(
                  children: [
                    Image.asset('assets/images/icon_india.png',height: 18,width: 18,),
                    SizedBox(width: 5,),
                    Text(
                      value.name,
                      style: const TextStyle(
                          color: Colors.black),
                    ),
                  ],
                ),
              ),
            )
                .toList(),
            onChanged: (value) {
              if (value == null) return;
              setState(() {
                _selectedCurrency = value;
              });
            },
            dropdownColor: Colors.white,
            icon: const Icon(
              Icons.arrow_drop_down,
              color: Colors.black, // <-- SEE HERE
            ),
          ),
        ),
      ],
    );
  }
}
