import 'package:flutter/material.dart';
import 'package:flutter_second_assignment/UI_Component_Design/First_Design.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'First Design',
      home: First_Design_Page(),
    );
  }
}
